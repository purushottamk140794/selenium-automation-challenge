package org.selenium.codingchallenge.Day01;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class WorldPopulationChallenge {
    static WebDriver driver;

    public static void main(String[] args) throws InterruptedException {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("https://www.worldometers.info/world-population/");

        String worldCurrentPopulation = "//*[@rel='current_population']";
        String todayThisYear_Population = "//div[text()='Today' or text()='This year']//parent::div//span[@class='rts-counter']";
        int count = 1;
        while (count <= 21) {
            System.out.println(+count + "per sec");
            if(count==21) break;
            System.out.println("-----------World Population---------------");
            printPopulationData(worldCurrentPopulation);
            System.out.println("-----------Today and This Year Population---------------");
            printPopulationData(todayThisYear_Population);
            Thread.sleep(1000);
            count++;
            System.out.println("==================================");

        }
    }


    public static void printPopulationData(String locator) throws InterruptedException {
        List<WebElement> populationList = driver.findElements(By.xpath(locator));
        for (WebElement e : populationList) {
            System.out.println(e.getText());
        }
    }
}
